class User {
  final String name;
  final String email;
  final String phone;

  User({
    this.email,
    this.name,
    this.phone,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': this.name,
      'email': this.email,
      'phone': this.phone,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return new User(
      name: map['name'] as String,
      email: map['email'] as String,
      phone: map['phone'] as String,
    );
  }
}
