import 'package:Provider_template_v1/res/consts.dart';
import 'package:Provider_template_v1/ui/pages/support/unknown_page.dart';
import 'package:flutter/material.dart';

import '../helpers/route_helper.dart';
import '../res/consts.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: EMPTY_STRING,
      home: UnknownPage(),
      locale: Locale(LOCALE_EN),
      supportedLocales: [
        Locale(LOCALE_EN),
      ],
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (RouteSettings settings) => RouteHelper.instance.onGenerateRoute(settings),
    );
  }
}
